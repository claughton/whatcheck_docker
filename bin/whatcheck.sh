#!/bin/bash
# run whatcheck and convert output to pdf
#
/opt/whatcheck/bin/whatcheck "$@" && latex pdbout && dvipdfm pdbout
