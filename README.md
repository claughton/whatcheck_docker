# whatcheck_docker

This repo provides [WHAT_CHECK](https://swift.cmbi.umcn.nl/gv/whatcheck/) via a Docker container.

## Installation

Easiest via pip:

```
pip install git+https://bitbucket.org/claughton/whatcheck_docker.git@v15.0
```
This installs one script in your path:

* whatcheck : The standard `whatcheck` command, but including automatic generation of the results file in pdf format.

## Usage

The `whatcheck` command works just like the normal installed version of the software:

```
$ whatcheck <pdbfile>
```
Amongst the output files the key results are in `pdbout.txt` and `pdbout.pdf`.

## Rebuilding the Docker image:

If you want to build your own copy of the image you will need to obtain your own copy of the compressed tarball for whatcheck from [here](https://swift.cmbi.umcn.nl/gv/whatcheck/). Place the file in the whatcheck_docker directory, next edit the Makefile to set your own DockerHub username, then:
```
% make build
% make publish
```

## License

Please see the web pages for [WHAT_CHECK](https://swift.cmbi.umcn.nl/gv/whatcheck/) for the conditions of use.

