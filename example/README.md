## WHATCHECK test

Command is:
```
% whatcheck 2oz2.pdb
```

Amongst the output files you should find `pdbout.txt`, and the pdf version of this, including graphs, `pdbout.pdf`.
