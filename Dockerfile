FROM ubuntu:latest
RUN echo "deb http://mirrors.kernel.org/ubuntu/ xenial main" >> /etc/apt/sources.list
RUN apt-get update && apt-get install -y dssp make gfortran texlive libpng12-0 transfig
RUN mkdir /opt/whatcheck
WORKDIR /opt/whatcheck
ADD whatcheck.tar.bz2 .
RUN make clean && make clean && make -j4
COPY bin/whatcheck.sh /usr/local/bin/whatcheck.sh
ENTRYPOINT ["/usr/local/bin/whatcheck.sh"]
CMD []
