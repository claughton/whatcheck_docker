PREFIX ?= /usr/local
VERSION = "15.0"

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/whatcheck $(DESTDIR)$(PREFIX)/bin/whatcheck

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/whatcheck
	@docker rmi claughton/whatcheck:$(VERSION)

build: 
	@docker build -t claughton/whatcheck:$(VERSION) -f Dockerfile . 

publish: build
	@docker push claughton/whatcheck:$(VERSION) 

.PHONY: all install uninstall build publish 
